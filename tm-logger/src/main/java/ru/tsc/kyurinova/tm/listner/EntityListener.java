package ru.tsc.kyurinova.tm.listner;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class EntityListener implements MessageListener {

    @NotNull
    private final LoggerService loggerService;

    @NotNull
    public EntityListener(@NotNull final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String json = textMessage.getText();
        loggerService.log(json);
    }

}
