package ru.tsc.kyurinova.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonRootName("domain")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @NotNull
    @JsonProperty("user")
    @XmlElement(name = "users")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    private List<UserDTO> users;

    @NotNull
    @JsonProperty("project")
    @XmlElement(name = "projects")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<ProjectDTO> projects;

    @NotNull
    @JsonProperty("task")
    @XmlElement(name = "tasks")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<TaskDTO> tasks;

}
