package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IAdminDataEndpoint;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.dto.Domain;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    public AdminDataEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    Domain getDomain(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getAdminDataService().getDomain();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void setDomain(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "domain", partName = "domain")
                    Domain domain) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().setDomain(domain);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBackupLoad(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataBackupLoad();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBackupSave(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataBackupSave();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBase64Load(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataBase64Load();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBase64Save(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataBase64Save();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBinaryLoad(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataBinaryLoad();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBinarySave(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataBinarySave();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataJsonLoadFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonLoadJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataJsonLoadJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataJsonSaveFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonSaveJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataJsonSaveJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataXmlLoadFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlLoadJaxBC(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataXmlLoadJaxBC();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataXmlSaveFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlSaveJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataXmlSaveJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataYamlLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataYamlLoadFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataYamlSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getAdminDataService().dataYamlSaveFasterXML();
    }
}
