package ru.tsc.kyurinova.tm.service.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.model.ISessionRepository;
import ru.tsc.kyurinova.tm.api.repository.model.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.api.service.model.ISessionService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIndexException;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.model.SessionRepository;
import ru.tsc.kyurinova.tm.repository.model.UserRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Session open(@NotNull final String login, @NotNull final String password) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return sign(session);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String salt = serviceLocator.getPropertyService().getSessionSecret();
        final int cycle = serviceLocator.getPropertyService().getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public boolean exists(@NotNull final String sessionId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final Session session1 = sessionRepository.findById(sessionId);
        if (session1 != null) return true;
        else return false;
    }

    @Override
    public void validate(@NotNull final Session session) {
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId() == null || session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final Session sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (!exists(session.getId())) throw new AccessDeniedException();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }

    }

    @SneakyThrows
    @Override
    public void validate(@NotNull Session session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUser().getId();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void remove(@Nullable final Session entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            return sessionRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            return sessionRepository.findById(id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            return sessionRepository.findByIndex(index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            return sessionRepository.getSize();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

}
